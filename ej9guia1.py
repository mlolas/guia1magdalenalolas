import math # incluye la función truncar

valorxh = int(input("Ingrese el valor a cobrar por hora: $"))
horas = float(input("Ingrese el número de horas: "))

if  math.trunc(horas) == horas: # si las horas truncadas = horas el valor no cambia
    hcobradas = horas
else:
    hcobradas = math.trunc(horas) + 1 # si las horas truncadas no son = horas se le + 1
cobro = hcobradas * valorxh # calcula el cobro final

print("El total a pagar es de $",cobro,"por las",hcobradas,"horas estacionadas") # imprime el resultado