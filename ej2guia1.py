# a = número de trajes
a = int(input("Ingrese el número de trajes que desea comprar (mínimo 3): "))

# condición para que a > 3
if a >= 3:
    for valor in range(a): # ciclo del número de trajes
        valor = int(input("Ingrese el valor de su traje: $"))
        if valor > 10000: # descuento del 15% por compras sobre $10000
            descuento = valor * 0.15
            print("El precio a pagar por su traje es: $", valor-descuento)
            print("Su descuento es de: $", descuento) 
        else: # descuento del 8% por compras bajo $10000
            descuento = valor * 0.08
            print("El precio a pagar por su traje es: $", valor-descuento)
            print("Su descuento es de: $", descuento)
else: # condición si esque a < 3
    print("Debe comprar mínimo 3 trajes")