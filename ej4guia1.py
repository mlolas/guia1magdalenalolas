# promedio usando while
N = int(input("Ingrese la cantidad de alumnos: "))
a = N
suma = 0

while N > 0: # ciclo while
    N = N - 1
    edad = int(input("Ingrese su edad: "))
    suma = suma + edad
print("El promedio de las edades de los alumnos es: ", suma/a) # calcula e imprime el promedio

# promedio usando for
M = int(input("Ingrese la cantidad de alumnos: "))
suma2 = 0

for i in range(M): # ciclo for
    edad2 = int(input("Ingrese su edad: "))
    suma2 = suma2 + edad2
print("El promedio de las edades de los alumnos es: ", suma2/M) # calcula e imprime el promedio